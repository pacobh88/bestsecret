package fjbermudez.es.products.data.remote.mapper.productsdetail

import fjbermudez.es.products.data.repository.remote.entities.ProductDetail
import fjbermudez.es.products.data.repository.remote.mapper.productsdetail.ProductsDetailMapper
import org.junit.Assert
import org.junit.Test

class ProductsDetailMapperTest {

    @Test
    fun productsDetailMapperTest(){
        //region productDetail
        val productDetail = ProductDetail(
            1,
            "shirts",
            "Tommy Hilfiger shirt description",
            "Tommy Hilfiger",
            80,
            "€",
            20,
            "https://picture.bestsecret.com/static/images/1041/image_31394462_20_620x757_0.jpg",
            4)
        //endregion

        val productDetailMapper = ProductsDetailMapper()
        val productDetailModel = productDetailMapper.fromResponse(productDetail)

        Assert.assertEquals(productDetailModel.id,1)
        Assert.assertEquals(productDetailModel.brand,"Tommy Hilfiger")
        Assert.assertEquals(productDetailModel.price,80)
        Assert.assertEquals(productDetailModel.currency,"€")
        Assert.assertEquals(productDetailModel.image,
            "https://picture.bestsecret.com/static/images/1041/image_31394462_20_620x757_0.jpg")
        Assert.assertEquals(productDetailModel.description,
            "Tommy Hilfiger shirt description")
        Assert.assertEquals(productDetailModel.discountPercentage,20)
        Assert.assertEquals(productDetailModel.stock,4)
    }
}