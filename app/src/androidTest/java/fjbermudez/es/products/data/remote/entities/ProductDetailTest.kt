package fjbermudez.es.products.data.remote.entities

import fjbermudez.es.products.data.repository.remote.entities.ProductDetail
import org.junit.Assert
import org.junit.Test

class ProductDetailTest {

    private val productDetail = ProductDetail(
        1,
        "shirts",
        "Tommy Hilfiger shirt description",
        "Tommy Hilfiger",
        80,
        "€",
        20,
        "https://picture.bestsecret.com/static/images/1041/image_31394462_20_620x757_0.jpg",
        4)

    @Test
    fun productDetailEntityTest() {

        Assert.assertEquals(productDetail.id,1)
        Assert.assertEquals(productDetail.brand,"Tommy Hilfiger")
        Assert.assertEquals(productDetail.price,80)
        Assert.assertEquals(productDetail.currency,"€")
        Assert.assertEquals(productDetail.image,
            "https://picture.bestsecret.com/static/images/1041/image_31394462_20_620x757_0.jpg")
        Assert.assertEquals(productDetail.description,
            "Tommy Hilfiger shirt description")
        Assert.assertEquals(productDetail.discountPercentage,20)
        Assert.assertEquals(productDetail.stock,4)
    }
}