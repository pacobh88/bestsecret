package fjbermudez.es.products.data.remote.mapper.products

import fjbermudez.es.products.data.repository.remote.entities.Product
import fjbermudez.es.products.data.repository.remote.entities.ProductsPage
import fjbermudez.es.products.data.repository.remote.mapper.products.ProductsPageMapper
import org.junit.Assert
import org.junit.Test

class ProductsPageMapperTest {

    @Test
    fun productsPageMapperTest(){
        //region productPage
        //region list
        var list = ArrayList<Product>()
        val product1 = Product(
            1,
            "shirts",
            "Tommy Hilfiger",
            80,
            "€",
            "https://picture.bestsecret.com/static/images/1041/image_31394462_20_620x757_0.jpg",
            "http://bestsecret-recruitment-api.herokuapp.com/products/1",
            "product")
        val product2 = Product(
            3,
            "shirts",
            "Ralph Lauren",
            65,
            "€",
            "https://picture.bestsecret.com/static/images/1101/image_31253398_20_620x757_0.jpg",
            "http://bestsecret-recruitment-api.herokuapp.com/products/2",
            "product")
        val product3 = Product(
            3,
            "shirts",
            "Calvin Klen",
            50,
            "€",
            "https://picture.bestsecret.com/static/images/907/image_31291071_33_620x757_0.jpg",
            "http://bestsecret-recruitment-api.herokuapp.com/products/3",
            "product")

        list.add(product1)
        list.add(product2)
        list.add(product3)
        //endregion
        val page = 1
        val pageSize = 3
        val size = "20"
        val link = "http://bestsecret-recruitment-api.herokuapp.com/products?page=1&pageSize=5"
        val type = "products"
        val next= "http://bestsecret-recruitment-api.herokuapp.com/products?page=2&pageSize=3"
        var productPage = ProductsPage(list,page,pageSize,size,link,type,next)
        //endregion

        val productsPageMapper = ProductsPageMapper()
        val productPageModel =  productsPageMapper.fromResponse(productPage)

        Assert.assertEquals(productPageModel.page,1)
        Assert.assertEquals(productPageModel.pageSize,3)
        Assert.assertEquals(productPageModel._link,link)
        Assert.assertEquals(productPageModel._type,type)
        Assert.assertEquals(productPageModel._next, next)
    }
}