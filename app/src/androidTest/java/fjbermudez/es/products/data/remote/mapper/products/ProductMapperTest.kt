package fjbermudez.es.products.data.remote.mapper.products

import fjbermudez.es.products.data.repository.remote.entities.Product
import fjbermudez.es.products.data.repository.remote.mapper.products.ProductMapper
import org.junit.Assert
import org.junit.Test

class ProductMapperTest {

    @Test
    fun productMapperTest(){
        //region product
        val product = Product(
            1,
            "shirts",
            "Tommy Hilfiger",
            80,
            "€",
            "https://picture.bestsecret.com/static/images/1041/image_31394462_20_620x757_0.jpg",
            "http://bestsecret-recruitment-api.herokuapp.com/products/1",
            "product")
        //endregion

        val productMapper = ProductMapper()

        val productModel = productMapper.fromResponse(product)

        Assert.assertEquals(productModel.id,1)
        Assert.assertEquals(productModel.brand,"Tommy Hilfiger")
        Assert.assertEquals(productModel.price,80)
        Assert.assertEquals(productModel.currency,"€")
        Assert.assertEquals(productModel.image,
            "https://picture.bestsecret.com/static/images/1041/image_31394462_20_620x757_0.jpg")
        Assert.assertEquals(productModel._link,
            "http://bestsecret-recruitment-api.herokuapp.com/products/1")
        Assert.assertEquals(productModel._type,"product")
    }
}