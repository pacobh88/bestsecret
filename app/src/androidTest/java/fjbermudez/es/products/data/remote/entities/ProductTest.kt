package fjbermudez.es.products.data.remote.entities

import fjbermudez.es.products.data.repository.remote.entities.Product
import org.junit.Assert
import org.junit.Test

class ProductTest {

    private val product = Product(
        1,
        "shirts",
        "Tommy Hilfiger",
        80,
        "€",
        "https://picture.bestsecret.com/static/images/1041/image_31394462_20_620x757_0.jpg",
        "http://bestsecret-recruitment-api.herokuapp.com/products/1",
        "product")

    @Test
    fun productEntityTest() {

      Assert.assertEquals(product.id,1)
      Assert.assertEquals(product.brand,"Tommy Hilfiger")
      Assert.assertEquals(product.price,80)
      Assert.assertEquals(product.currency,"€")
      Assert.assertEquals(product.image,
          "https://picture.bestsecret.com/static/images/1041/image_31394462_20_620x757_0.jpg")
      Assert.assertEquals(product._link,
          "http://bestsecret-recruitment-api.herokuapp.com/products/1")
      Assert.assertEquals(product._type,"product")
    }
}