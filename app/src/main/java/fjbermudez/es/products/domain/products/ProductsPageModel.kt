package fjbermudez.es.products.domain.products

import fjbermudez.es.products.data.repository.remote.base.ResponseBase


data class ProductsPageModel(
    var list: List<ProductModel> = ArrayList(),
    var page: Int = 0,
    var pageSize: Int = 0,
    var size: String = "",
    var _link: String = "",
    var _type: String = "",
    var _next: String? = "") :ResponseBase()
