package fjbermudez.es.products.domain.productsdetail

import fjbermudez.es.products.data.repository.remote.base.ResponseBase

data class ProductDetailModel(
    val id: Int,
    val name: String,
    val description: String,
    val brand: String,
    val price: Int,
    val currency: String,
    val discountPercentage: Int,
    val image: String,
    val stock: Int): ResponseBase()