package fjbermudez.es.products.domain.products

import java.io.Serializable

data class ProductModel(
    var id: Int,
    var name: String,
    var brand: String,
    var price: Long,
    var currency: String,
    var image: String,
    var _link: String,
    var _type: String): Serializable