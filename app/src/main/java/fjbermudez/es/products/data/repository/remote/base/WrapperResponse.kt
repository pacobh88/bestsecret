package fjbermudez.es.products.data.repository.remote.base

data class WrapperResponse <T>(val responseBase:ResponseBase,
                               val errorBase:ErrorBase)