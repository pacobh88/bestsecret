package fjbermudez.es.products.data.repository.remote

import fjbermudez.es.products.data.repository.remote.entities.ProductDetail
import fjbermudez.es.products.data.repository.remote.entities.ProductsPage
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiServices {

    @GET("products")
    fun getProducts(@Query("page")page:Int,@Query("pageSize")pageSize:Int): Call<ProductsPage>

    @GET("products/{productId}")
    fun getProductDetail(@Path("productId") productId: Int): Call<ProductDetail>
}