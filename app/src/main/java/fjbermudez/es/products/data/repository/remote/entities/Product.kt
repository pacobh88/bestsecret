package fjbermudez.es.products.data.repository.remote.entities

import com.google.gson.annotations.SerializedName
import fjbermudez.es.products.data.repository.remote.base.ResponseBase

data class Product(
    @SerializedName("id")
    val id: Int,
    @SerializedName("name")
    val name: String,
    @SerializedName("brand")
    val brand: String,
    @SerializedName("price")
    val price: Long,
    @SerializedName("currency")
    val currency: String,
    @SerializedName("image")
    val image: String,
    @SerializedName("_link")
    val _link: String,
    @SerializedName("_type")
    val _type: String): ResponseBase()