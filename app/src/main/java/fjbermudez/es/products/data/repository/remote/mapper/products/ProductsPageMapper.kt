package fjbermudez.es.products.data.repository.remote.mapper.products

import fjbermudez.es.products.data.repository.remote.entities.ProductsPage
import fjbermudez.es.products.data.repository.remote.mapper.base.ResponseMapper
import fjbermudez.es.products.domain.products.ProductModel
import fjbermudez.es.products.domain.products.ProductsPageModel

class ProductsPageMapper:ResponseMapper<ProductsPage, ProductsPageModel> {

    override fun fromResponse(productsPage: ProductsPage): ProductsPageModel {


        var productsListModel = ArrayList<ProductModel>()

        productsPage.list.let { it ->
            var productsMapper = ProductMapper()

            it?.forEach {
                  productsListModel.add(productsMapper.fromResponse(it))
            }
        }

        return ProductsPageModel(
            productsListModel,
            productsPage.page,
            productsPage.pageSize,
            productsPage.size,
            productsPage._link,
            productsPage._type,
            productsPage._next)
    }
}