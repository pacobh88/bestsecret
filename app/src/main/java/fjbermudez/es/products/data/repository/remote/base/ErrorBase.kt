package fjbermudez.es.products.data.repository.remote.base

import java.io.Serializable

class ErrorBase:Serializable {

    /**
     * Error Code
     */
    lateinit var errorCode: String

    /**
     * Message error
     */
    var errorMessage: String?

    /**
     *
     */
    var errorReason: ERROR_REASON? = null

    constructor(errorCode: String, errorMessage: String?, errorReason:ERROR_REASON) {
        this.errorReason = errorReason
        this.errorCode = errorCode
        this.errorMessage = errorMessage
    }
    enum class ERROR_REASON {

        NO_ERROR,
        RESPONSE_ERROR,
        UNEXPECTED
    }

}