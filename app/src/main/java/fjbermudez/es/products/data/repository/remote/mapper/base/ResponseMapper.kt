package fjbermudez.es.products.data.repository.remote.mapper.base

interface ResponseMapper<E, M> {

    fun fromResponse(entity: E): M

}