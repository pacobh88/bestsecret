package fjbermudez.es.products.data.repository

import androidx.lifecycle.MutableLiveData
import fjbermudez.es.products.data.repository.remote.IRemoteDataSource
import fjbermudez.es.products.data.repository.remote.base.WrapperResponse
import fjbermudez.es.products.domain.products.ProductsPageModel
import fjbermudez.es.products.domain.productsdetail.ProductDetailModel

class DataProvider private constructor(remoteDataSource: IRemoteDataSource){

    //IRemoteDataSource
    val remoteDataSource = remoteDataSource

    //Singleton
    companion object {
        private  var instance: DataProvider? = null

        fun getInstance(remoteDataSource: IRemoteDataSource): DataProvider {

            if(instance == null){
                instance = DataProvider(remoteDataSource)
            }
            return instance as DataProvider
        }
    }


    fun getProducts(page:Int, pageSize:Int): MutableLiveData<WrapperResponse<ProductsPageModel>> {
        return remoteDataSource.getProducts(page,pageSize)
    }

    fun getProductDetail(id:Int):MutableLiveData<WrapperResponse<ProductDetailModel>>{
       return remoteDataSource.getProductDetail(id)
    }
}