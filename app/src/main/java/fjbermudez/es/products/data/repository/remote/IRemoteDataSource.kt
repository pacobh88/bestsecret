package fjbermudez.es.products.data.repository.remote

import androidx.lifecycle.MutableLiveData
import fjbermudez.es.products.data.repository.remote.base.WrapperResponse
import fjbermudez.es.products.domain.products.ProductsPageModel
import fjbermudez.es.products.domain.productsdetail.ProductDetailModel

interface IRemoteDataSource {

    fun getProducts(page:Int,pageSize:Int):MutableLiveData<WrapperResponse<ProductsPageModel>>

    fun getProductDetail(id: Int):MutableLiveData<WrapperResponse<ProductDetailModel>>
}