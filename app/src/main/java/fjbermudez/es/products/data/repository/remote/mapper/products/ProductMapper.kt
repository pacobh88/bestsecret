package fjbermudez.es.products.data.repository.remote.mapper.products

import fjbermudez.es.products.data.repository.remote.entities.Product
import fjbermudez.es.products.data.repository.remote.mapper.base.ResponseMapper
import fjbermudez.es.products.domain.products.ProductModel

class ProductMapper:ResponseMapper<Product,ProductModel> {

    override fun fromResponse(product: Product): ProductModel {

        return ProductModel(
            product.id,
            product.name,
            product.brand,
            product.price,
            product.currency,
            product.image,
            product._link,
            product._type
        )
    }
}