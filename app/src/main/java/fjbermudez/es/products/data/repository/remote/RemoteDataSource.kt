package fjbermudez.es.products.data.repository.remote

import androidx.lifecycle.MutableLiveData
import fjbermudez.es.products.BuildConfig
import fjbermudez.es.products.BuildConfig.BASE_URL
import fjbermudez.es.products.data.repository.remote.base.ErrorBase
import fjbermudez.es.products.data.repository.remote.base.ResponseBase
import fjbermudez.es.products.data.repository.remote.base.WrapperResponse
import fjbermudez.es.products.data.repository.remote.entities.ProductDetail
import fjbermudez.es.products.data.repository.remote.entities.ProductsPage
import fjbermudez.es.products.data.repository.remote.mapper.products.ProductsPageMapper
import fjbermudez.es.products.data.repository.remote.mapper.productsdetail.ProductsDetailMapper
import fjbermudez.es.products.domain.products.ProductsPageModel
import fjbermudez.es.products.domain.productsdetail.ProductDetailModel
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit

class RemoteDataSource : IRemoteDataSource {
    val TIMEOUT: Long = 30

    var apiServices: ApiServices

    init {
        val httpClient : OkHttpClient.Builder = OkHttpClient.Builder()
        httpClient.connectTimeout(TIMEOUT, TimeUnit.SECONDS)
        httpClient.readTimeout(TIMEOUT, TimeUnit.SECONDS)
        httpClient.writeTimeout(TIMEOUT, TimeUnit.SECONDS)

        httpClient.addInterceptor { chain ->
            val original = chain.request()

            // Request customization: add request headers
            val requestBuilder = original.newBuilder()
                .addHeader("Accept", "application/json")
                .addHeader("Authorization","ddf49ca9-44cf-4613-b218-ddc030bbfa63")

            val request = requestBuilder.build()
            chain.proceed(request)
        }

        if (BuildConfig.DEBUG) {
            // Adding interceptor to debug

            httpClient.addInterceptor(createLoggingInterceptor())
        }

        val retrofit: Retrofit = Retrofit.Builder()
            .callbackExecutor(Executors.newSingleThreadExecutor())
            .client(httpClient.build())
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(BASE_URL)
            .build()

        apiServices = retrofit.create(ApiServices::class.java)
    }

    /**
     * Method to create logging interceptor
     */
    private fun createLoggingInterceptor(): Interceptor {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        return logging
    }

    // ProductsList
    override fun getProducts(page: Int, pageSize: Int):MutableLiveData<WrapperResponse<ProductsPageModel>> {

        val productCall = apiServices.getProducts(page,pageSize)

        val productsPageResponse = MutableLiveData<WrapperResponse<ProductsPageModel>>()
        productCall.enqueue(object : Callback<ProductsPage> {

            override fun onResponse(call: Call<ProductsPage>, response: Response<ProductsPage>) {
                if(response.isSuccessful && response.body() !=null){

                    productsPageResponse.postValue(WrapperResponse(ProductsPageMapper().fromResponse(
                            response.body()!!), ErrorBase("","",
                        ErrorBase.ERROR_REASON.NO_ERROR)
                    ))
                }else{
                    productsPageResponse.postValue(WrapperResponse(ResponseBase(),ErrorBase("",response.message(),
                        ErrorBase.ERROR_REASON.RESPONSE_ERROR)))
                }
            }

            override fun onFailure(call: Call<ProductsPage>, t: Throwable) {
                productsPageResponse.postValue(WrapperResponse(ResponseBase(),ErrorBase("",t.message,
                    ErrorBase.ERROR_REASON.UNEXPECTED)))

            }


        })

        return productsPageResponse
    }

    //ProductDetail
    override fun getProductDetail(id: Int):MutableLiveData<WrapperResponse<ProductDetailModel>> {

        val productDetailCall = apiServices.getProductDetail(id)

        val productDetailResponse = MutableLiveData<WrapperResponse<ProductDetailModel>>()

        productDetailCall.enqueue(object : Callback<ProductDetail> {

            override fun onResponse(
                call: Call<ProductDetail>,
                response: Response<ProductDetail>
            ) {
                if (response.isSuccessful && response.body() != null) {

                    productDetailResponse.postValue(
                        WrapperResponse(
                            ProductsDetailMapper().fromResponse(
                                response.body()!!
                            ), ErrorBase("","",ErrorBase.ERROR_REASON.NO_ERROR))
                    )
                } else {
                    productDetailResponse.postValue(WrapperResponse(ResponseBase(),ErrorBase("",
                        response.message(),ErrorBase.ERROR_REASON.RESPONSE_ERROR)))
                }
            }

            override fun onFailure(call: Call<ProductDetail>, t: Throwable) {
                productDetailResponse.postValue(WrapperResponse(ResponseBase(),ErrorBase("",t.message,
                    ErrorBase.ERROR_REASON.UNEXPECTED)))
            }
        })

        return  productDetailResponse
    }

}