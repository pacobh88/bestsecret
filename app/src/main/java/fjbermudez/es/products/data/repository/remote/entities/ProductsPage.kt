package fjbermudez.es.products.data.repository.remote.entities

import com.google.gson.annotations.SerializedName
import fjbermudez.es.products.data.repository.remote.base.ResponseBase

data class ProductsPage(
    @SerializedName("list")
    val list: List<Product>? = null,
    @SerializedName("page")
    val page: Int,
    @SerializedName("pageSize")
    val pageSize: Int,
    @SerializedName("size")
    val size: String,
    @SerializedName("_link")
    val _link: String,
    @SerializedName("_type")
    val _type: String,
    @SerializedName("_next")
    val _next: String?):ResponseBase()