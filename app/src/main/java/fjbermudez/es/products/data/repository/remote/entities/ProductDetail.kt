package fjbermudez.es.products.data.repository.remote.entities

import com.google.gson.annotations.SerializedName
import fjbermudez.es.products.data.repository.remote.base.ResponseBase

data class ProductDetail(
    @SerializedName("id")
    val id: Int,
    @SerializedName("name")
    val name: String,
    @SerializedName("description")
    val description: String,
    @SerializedName("brand")
    val brand: String,
    @SerializedName("price")
    val price: Int,
    @SerializedName("currency")
    val currency: String,
    @SerializedName("discountPercentage")
    val discountPercentage: Int,
    @SerializedName("image")
    val image: String,
    @SerializedName("stock")
    val stock: Int):ResponseBase()