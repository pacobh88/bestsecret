package fjbermudez.es.products.data.repository.remote.mapper.productsdetail

import fjbermudez.es.products.data.repository.remote.entities.ProductDetail
import fjbermudez.es.products.data.repository.remote.mapper.base.ResponseMapper
import fjbermudez.es.products.domain.productsdetail.ProductDetailModel

class ProductsDetailMapper:ResponseMapper<ProductDetail,ProductDetailModel> {

    override fun fromResponse(productDetail: ProductDetail): ProductDetailModel {

        return ProductDetailModel(
            productDetail.id,
            productDetail.name,
            productDetail.description,
            productDetail.brand,
            productDetail.price,
            productDetail.currency,
            productDetail.discountPercentage,
            productDetail.image,
            productDetail.stock)
    }
}