package fjbermudez.es.products

import fjbermudez.es.products.app.base.ViewModelFactory
import fjbermudez.es.products.data.repository.DataProvider
import fjbermudez.es.products.data.repository.remote.RemoteDataSource

object Injector{

    fun provideDataSource(): DataProvider {
        return DataProvider.getInstance(RemoteDataSource())
    }
    fun provideViewModelFactory(): ViewModelFactory {
        return ViewModelFactory(provideDataSource())
    }

}