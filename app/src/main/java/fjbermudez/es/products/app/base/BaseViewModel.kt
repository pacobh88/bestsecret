package fjbermudez.es.products.app.base

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import fjbermudez.es.products.data.repository.remote.base.ErrorBase

open class BaseViewModel:ViewModel(){

    protected var productErrorLiveData = MutableLiveData<ErrorBase>()
    protected var loadingLiveData: MutableLiveData<Boolean> = MutableLiveData()

    fun setLoading(value: Boolean) {
        loadingLiveData.postValue(value)
    }

}