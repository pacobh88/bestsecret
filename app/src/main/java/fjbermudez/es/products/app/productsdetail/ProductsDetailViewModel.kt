package fjbermudez.es.products.app.productsdetail

import androidx.lifecycle.MutableLiveData
import fjbermudez.es.products.app.base.BaseViewModel
import fjbermudez.es.products.data.repository.DataProvider
import fjbermudez.es.products.data.repository.remote.base.ErrorBase
import fjbermudez.es.products.data.repository.remote.base.WrapperResponse
import fjbermudez.es.products.domain.productsdetail.ProductDetailModel

class ProductsDetailViewModel(private val dataProvider: DataProvider):BaseViewModel() {


    var productDetailView = MutableLiveData<ProductDetailModel>()
    private var productDetailData: MutableLiveData<WrapperResponse<ProductDetailModel>> = MutableLiveData()

    fun getProductDetail(productId: Int) {

        setLoading(true)
        productDetailData = dataProvider.getProductDetail(productId)

        productDetailData.observeForever {
            productDetailData?.let {

                if(it.value?.errorBase?.errorReason == ErrorBase.ERROR_REASON.NO_ERROR) {

                    val productDetailModel: ProductDetailModel = it.value!!.responseBase as ProductDetailModel

                    productDetailView.postValue(productDetailModel)

                }else{

                    productErrorLiveData.postValue(it.value?.errorBase)

                }

                setLoading(false)
            }
        }
    }

    fun getErrorLiveData(): MutableLiveData<ErrorBase> {
        return productErrorLiveData
    }

    fun getLoading(): MutableLiveData<Boolean> {
        return  loadingLiveData
    }
}