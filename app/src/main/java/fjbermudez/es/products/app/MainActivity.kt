package fjbermudez.es.products.app

import android.os.Bundle
import android.view.View
import fjbermudez.es.products.R
import fjbermudez.es.products.app.base.BaseActivity
import fjbermudez.es.products.app.products.ProductsFragment
import fjbermudez.es.products.app.productsdetail.ProductsDetailFragment
import kotlinx.android.synthetic.main.main_activity.*

class MainActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                    .replace(R.id.container, ProductsFragment.newInstance())
                    .commitNow()
        }
    }

    override fun getLayout(): Int {
        return R.layout.main_activity
    }

    fun configureToolbar(title: String, visibilityImage: Boolean) {
        tvToolbar.text = title
        if(visibilityImage){
            ivToolbar.visibility = View.VISIBLE
            ivToolbar.setOnClickListener {
                onBackPressed()
            }
        }else{
            ivToolbar.visibility = View.INVISIBLE
        }
    }

    fun onClickProduct(idProduct:Int) {
        goToProductDetail(idProduct)
    }

    private fun goToProductDetail(idProduct:Int) {
        supportFragmentManager.beginTransaction()
            .replace(R.id.container, ProductsDetailFragment.newInstance(idProduct))
            .addToBackStack(ProductsDetailFragment.TAG)
            .commit()
    }
}
