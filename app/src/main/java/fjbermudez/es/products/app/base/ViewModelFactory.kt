package fjbermudez.es.products.app.base

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import fjbermudez.es.products.app.products.ProductsViewModel
import fjbermudez.es.products.app.productsdetail.ProductsDetailViewModel
import fjbermudez.es.products.data.repository.DataProvider

class ViewModelFactory(private val dataProvider: DataProvider) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        var viewmodel: ViewModel = BaseViewModel()
        if (modelClass.name == ProductsViewModel::class.java.name){
            viewmodel = ProductsViewModel(dataProvider)
        }else if (modelClass.name == ProductsDetailViewModel::class.java.name){
            viewmodel = ProductsDetailViewModel(dataProvider)
        }

        return viewmodel as T
    }
}