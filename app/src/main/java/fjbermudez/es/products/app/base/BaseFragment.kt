package fjbermudez.es.products.app.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import fjbermudez.es.products.data.repository.remote.base.ErrorBase

abstract class BaseFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(getLayout(), container, false)
        this.createView(inflater, container, savedInstanceState)
        return view
    }


    protected abstract fun createView(inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?)

    protected abstract fun getLayout(): Int

    protected abstract fun configureToolbar()

    protected abstract fun setupViewModel()

    protected abstract fun observeViewModel()

    protected abstract fun showError(error:ErrorBase)
    protected abstract fun showLoadingDialog(showLoading:Boolean)
}