package fjbermudez.es.products.app.products

import androidx.lifecycle.MutableLiveData
import fjbermudez.es.products.app.base.BaseViewModel
import fjbermudez.es.products.data.repository.DataProvider
import fjbermudez.es.products.data.repository.remote.base.ErrorBase
import fjbermudez.es.products.data.repository.remote.base.WrapperResponse
import fjbermudez.es.products.domain.products.ProductModel
import fjbermudez.es.products.domain.products.ProductsPageModel

class ProductsViewModel(private val dataProvider: DataProvider) : BaseViewModel() {

    var productsPageView = MutableLiveData<List<ProductModel>>()
    private var productsPageData: MutableLiveData<WrapperResponse<ProductsPageModel>> = MutableLiveData()

    private var actualPage = 0
    private var alreadyChecked = false
    private var productsList:ArrayList<ProductModel> = ArrayList()
    private var numTotalProducts = 0

    companion object {
        //Default size
        const val PAGE_SIZE = 5
    }
    fun getProducts(page:Int){
        if((page==1 && productsList.size == 0) || (page>1 && productsList.size>0)) {
            actualPage = page

            setLoading(true)
            productsPageData = dataProvider.getProducts(actualPage, PAGE_SIZE)

            productsPageData.observeForever {
                productsPageData?.let {

                    if (it.value?.errorBase?.errorReason == ErrorBase.ERROR_REASON.NO_ERROR) {

                        val productsPage: ProductsPageModel =
                            it.value!!.responseBase as ProductsPageModel

                        alreadyChecked = false
                        actualPage = productsPage.page
                        numTotalProducts = productsPage.size.toInt()
                        productsPage.list?.let { it1 -> productsList.addAll(it1) }
                        productsPageView.postValue(productsList)

                    } else {

                        productErrorLiveData.postValue(it.value?.errorBase)

                    }

                    setLoading(false)
                }
            }
        }
    }

//    private fun addElements(productsFromService: List<ProductModel>,
//                            productsList: ArrayList<ProductModel>) {
//
//        if(productsList.size>0) {
//            productsFromService.forEach {
//                val productFromService = it
//                productsList.forEach {
//                    val productList = it
//                    if (productFromService != productList) {
//                        this.productsList.add(productFromService)
//                    }
//                }
//            }
//        }else{
//            this.productsList.addAll(productsFromService)
//        }
//
//
//
//    }

    fun checkNewPage(){

        if (numTotalProducts > productsList.size && !alreadyChecked) {
            alreadyChecked = true
            getProducts(actualPage+1)

        }

    }

    fun getErrorLiveData(): MutableLiveData<ErrorBase> {
        return productErrorLiveData
    }

    fun getLoading(): MutableLiveData<Boolean> {
        return  loadingLiveData
    }


}
