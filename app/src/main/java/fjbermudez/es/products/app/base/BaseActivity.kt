package fjbermudez.es.products.app.base

import android.os.Build
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import fjbermudez.es.products.R
import fjbermudez.es.products.data.repository.remote.base.ErrorBase
import kotlinx.android.synthetic.main.main_activity.*


abstract class BaseActivity : AppCompatActivity() {


    protected abstract fun getLayout(): Int

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //if api < 23 change color to black, else windowLightStatus
        if(Build.VERSION.SDK_INT<Build.VERSION_CODES.M) {
            // clear FLAG_TRANSLUCENT_STATUS flag:
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            // finally change the color
            window.statusBarColor = (ContextCompat.getColor(this, R.color.black))
        }
        setContentView(getLayout())
    }

    fun manageError(error:ErrorBase){

        val errorDialog: AlertDialog

        val builder = AlertDialog.Builder(this,R.style.AlertDialogStyle)

        // Set the alert dialog title
        builder.setTitle(getString(R.string.error_title))

        // Display a message on alert dialog
        builder.setMessage(error.errorMessage)

        // Set a positive button and its click listener on alert dialog
        builder.setPositiveButton(getString(R.string.accept)){ dialog, which ->
            dialog.dismiss()
            onBackPressed()

        }
        errorDialog = builder.create()
        errorDialog.show()
    }

    fun showLoading(showLoading: Boolean) {

        if(showLoading){
            rlLoading.visibility = View.VISIBLE
        }else{
            rlLoading.visibility = View.GONE
        }
    }
}