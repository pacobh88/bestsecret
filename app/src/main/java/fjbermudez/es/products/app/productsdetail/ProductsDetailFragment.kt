package fjbermudez.es.products.app.productsdetail

import android.graphics.PorterDuff
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.bumptech.glide.Glide
import fjbermudez.es.products.Injector
import fjbermudez.es.products.R
import fjbermudez.es.products.app.MainActivity
import fjbermudez.es.products.app.base.BaseFragment
import fjbermudez.es.products.app.utils.UiUtils
import fjbermudez.es.products.data.repository.remote.base.ErrorBase
import fjbermudez.es.products.domain.productsdetail.ProductDetailModel
import kotlinx.android.synthetic.main.product_detail_fragment.*

class ProductsDetailFragment: BaseFragment() {


    companion object {

        val TAG: String = ProductsDetailFragment.javaClass.name

        private const val PRODUCT_ID = "product_id"
        private const val SPACE = " "


        fun newInstance(productId:Int): ProductsDetailFragment {
            val fragment = ProductsDetailFragment()
            val args = Bundle()
            args.putInt(PRODUCT_ID, productId)
            fragment.arguments = args
            return fragment
        }

    }

    private lateinit var productDetailViewModel: ProductsDetailViewModel
    private lateinit var productSelected: ProductDetailModel;

    override fun createView(inflater: LayoutInflater, container: ViewGroup?,
                            savedInstanceState: Bundle?) {}


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        configureToolbar()
        setupViewModel()
        observeViewModel()
        setupListeners()
    }

    private fun setupListeners() {
        btAddToCart.setOnClickListener {

            UiUtils.showToastMessage(context,getMessageStock())

        }
    }

    /**
     * Method to get stock messsage
     */
    private fun getMessageStock():String{
        var messageToShow: String
        if(productSelected.stock>0){
            messageToShow = getString(R.string.message_not_implemented)
        }else{
            messageToShow = getString(R.string.message_no_stock)
        }
        return messageToShow

    }

    override fun getLayout(): Int {
        return R.layout.product_detail_fragment
    }

    override fun configureToolbar() {
        (activity as MainActivity).configureToolbar(getString(R.string.product_detail_title),true)
    }
    override fun setupViewModel() {
        productDetailViewModel = ViewModelProviders.of(this, Injector.provideViewModelFactory())
            .get(ProductsDetailViewModel::class.java)
    }

    override fun observeViewModel() {

        productDetailViewModel.productDetailView.observe(this, Observer {
            updateView(it)
        })

        productDetailViewModel.getErrorLiveData().observe(this, Observer {
            showError(it)
        })

        productDetailViewModel.getLoading().observe(this, Observer {
            showLoadingDialog(it)
        })
    }

    /**
     * Method to paint product detail view
     */
    private fun updateView(productDetailModel: ProductDetailModel) {


        productSelected = productDetailModel

        loadProductImage(productDetailModel.image)
        tvName.text = productDetailModel.name.capitalize()
        tvBrand.text = productDetailModel.brand
        tvDescription.text = productDetailModel.description

        if(productDetailModel.price>0){
            rlPrice.visibility = View.VISIBLE
            val price = productDetailModel.price.toString()+SPACE+productDetailModel.currency
            tvPrice.text = price
        }else{
            rlPrice.visibility = View.INVISIBLE
        }
        val discountedPrice = calculateDiscountedPrice(productDetailModel.price,
            productDetailModel.discountPercentage)+ SPACE+productDetailModel.currency

        tvDiscountedPrice.text = discountedPrice
        showImageSale(productDetailModel.discountPercentage)
        llProductDetailData.visibility = View.VISIBLE

    }

    /**
     * Method to calculate amount of money discounted
     */
    private fun calculateDiscountedPrice(price: Int, discountPercentage: Int): String {

        val priceDiscounted = price - ((price*discountPercentage)/100)

        return priceDiscounted.toString()
    }

    /**
     * Method to load product image
     */
    private fun loadProductImage(image: String) {
        val circularProgressDrawable = CircularProgressDrawable(activity?.applicationContext!!)
        circularProgressDrawable.strokeWidth = 5f
        circularProgressDrawable.centerRadius = 30f
        circularProgressDrawable.setColorFilter(
            ContextCompat.getColor(
                activity?.applicationContext!!,
                R.color.soft_orange
            ), PorterDuff.Mode.SRC_IN
        )

        circularProgressDrawable.start()

        Glide.with(activity?.applicationContext!!).load(Uri.parse(image)).placeholder(circularProgressDrawable)
            .into(ivImageProduct)
    }

    /**
     * Method to show or hide sale image
     * @param discountPercentage
     */
    private fun showImageSale(discountPercentage:Int){

       if(discountPercentage>30){
            ivSale.visibility = View.VISIBLE
        }else{
            ivSale.visibility = View.GONE
        }
    }

    /**
     * Method to call error dialog
     * @param error
     */
    override fun showError(error: ErrorBase) {
        (activity as MainActivity).manageError(error)
    }

    /**
     * Method to show or hide loadingDialog
     * @param showLoading
     */
    override fun showLoadingDialog(showLoading: Boolean) {
        (activity as MainActivity).showLoading(showLoading)
    }

    override fun onResume() {
        super.onResume()
        productDetailViewModel.getProductDetail(arguments?.get(PRODUCT_ID) as Int)

    }


}