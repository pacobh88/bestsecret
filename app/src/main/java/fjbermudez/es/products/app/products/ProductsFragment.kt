package fjbermudez.es.products.app.products

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import fjbermudez.es.products.Injector
import fjbermudez.es.products.R
import fjbermudez.es.products.app.MainActivity
import fjbermudez.es.products.app.base.BaseFragment
import fjbermudez.es.products.data.repository.remote.base.ErrorBase
import fjbermudez.es.products.domain.products.ProductModel
import kotlinx.android.synthetic.main.products_fragment.*

class ProductsFragment : BaseFragment(), ProductsAdapter.ItemClickListener {


    companion object {

        val TAG: String = ProductsFragment.javaClass.name

        fun newInstance() = ProductsFragment()
        const val INITIAL_PAGE = 1

    }

    private lateinit var productsViewModel: ProductsViewModel
    private lateinit var productsAdapter: ProductsAdapter

    override fun createView(inflater: LayoutInflater, container: ViewGroup?,
                            savedInstanceState: Bundle?) {
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        configureToolbar()
        setupViewModel()
        observeViewModel()
    }

    private fun initViews() {
        //region recyclerview
        rvProductList.layoutManager = GridLayoutManager(context,2)
        rvProductList.setHasFixedSize(false)
        productsAdapter = ProductsAdapter(activity as MainActivity)
        productsAdapter.productsList = ArrayList()
        productsAdapter.itemClickListener = this
        rvProductList.adapter = productsAdapter
        //endregion
        //region Scrooll listener

        rvProductList.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (isLastVisible(recyclerView)) {
                    productsViewModel.checkNewPage()
                }
            }
        })
        //endregion
    }

    override fun getLayout(): Int {
        return R.layout.products_fragment
    }

    override fun configureToolbar() {

        (activity as MainActivity).configureToolbar(getString(R.string.app_name),false)

    }

    override fun setupViewModel() {

        productsViewModel = ViewModelProviders.of(this, Injector.provideViewModelFactory())
            .get(ProductsViewModel::class.java)

    }

    override fun observeViewModel() {

        productsViewModel.productsPageView.observe(this, Observer {
            updateView(it)
        })

        productsViewModel.getErrorLiveData().observe(this, Observer {
            showError(it)
        })

        productsViewModel.getLoading().observe(this, Observer {
            showLoadingDialog(it)
        })
    }

    /**
     * Method  to check is last element of a page is completely visitble
     */
    private fun isLastVisible(recyclerView: RecyclerView): Boolean {

        val layoutManager = recyclerView.layoutManager as GridLayoutManager?
        var pos = 0
        if (layoutManager != null) {
            pos = layoutManager.findLastCompletelyVisibleItemPosition() + 1 // pos [0,4] numItems --> 5
        }
        val numItems = rvProductList.adapter?.itemCount

        return pos >= numItems!!
    }


    /**
     * Method to call error dialog
     * @param error
     */
    override fun showError(error: ErrorBase) {
        (activity as MainActivity).manageError(error)
    }

    /**
     * Method to show or hide loadingDialog
     * @param showLoading
     */
    override fun showLoadingDialog(showLoading: Boolean) {
        (activity as MainActivity).showLoading(showLoading)
    }



    /**
     * Method to paint a page of products
     */
    private fun updateView(list: List<ProductModel>?) {
        productsAdapter.updateList(list)
        productsAdapter.itemClickListener = this
    }

    override fun onItemClick(productModelId: Int) {
        (activity as MainActivity).onClickProduct(productModelId)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        productsViewModel.getProducts(INITIAL_PAGE)
    }

}
