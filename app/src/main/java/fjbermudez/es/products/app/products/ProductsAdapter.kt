package fjbermudez.es.products.app.products

import android.graphics.PorterDuff
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.bumptech.glide.Glide
import fjbermudez.es.products.R
import fjbermudez.es.products.app.MainActivity
import fjbermudez.es.products.domain.products.ProductModel
import kotlinx.android.synthetic.main.item_product.view.*

class ProductsAdapter (private val parentActivity: MainActivity) : RecyclerView.Adapter<ProductsAdapter.ProductsViewHolder>() {

    var productsList: ArrayList<ProductModel> = ArrayList()
    interface ItemClickListener{
        fun onItemClick(productModelId:Int)
    }
    public lateinit var itemClickListener:ItemClickListener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductsViewHolder {

        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_product, parent, false)


        return ProductsViewHolder(v)
    }

    override fun getItemCount(): Int {
        return productsList.size
    }

    class ProductsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val view = itemView
    }

    override fun onBindViewHolder(holder: ProductsViewHolder, position: Int) {
        with(holder.view) {



            if (productsList.size > 0) {

                //region OnClick
                holder.view.setOnClickListener {
                    itemClickListener.let {
                        it.onItemClick(productsList[position].id)
                    }
                }
                //endregion
                productsList?.get(position)?.let {

                    val circularProgressDrawable = CircularProgressDrawable(parentActivity)
                    circularProgressDrawable.strokeWidth = 5f
                    circularProgressDrawable.centerRadius = 30f
                    circularProgressDrawable.setColorFilter(
                        ContextCompat.getColor(
                            parentActivity,
                            R.color.soft_orange
                        ), PorterDuff.Mode.SRC_IN
                    )

                    circularProgressDrawable.start()

                    Glide.with(parentActivity).load(Uri.parse(it.image)).placeholder(circularProgressDrawable)
                        .into(ivProduct)

                    tvProductName.text = productsList[position].brand

                }
            }
        }

    }

    fun updateList(list: List<ProductModel>?) {
        productsList = list as ArrayList<ProductModel>
        notifyDataSetChanged()
    }
}