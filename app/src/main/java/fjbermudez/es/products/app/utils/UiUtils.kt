package fjbermudez.es.products.app.utils

import android.R.id.message
import android.content.Context
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import fjbermudez.es.products.R


class UiUtils {

    companion object{

        //Custom Toast message

        fun showToastMessage(context: Context?, messageToShow: String) {

            val toast = Toast.makeText(context, messageToShow, Toast.LENGTH_LONG)
            val view = toast.view
            //Change toast background
            view.background = ContextCompat.getDrawable(context!!,R.drawable.shape_button_white_orange_border)

            //Gets the TextView from the Toast so it can be editted
            val text = view.findViewById<TextView>(message)
            text.textAlignment = View.TEXT_ALIGNMENT_CENTER
            text.setTextColor(ContextCompat.getColor(context, R.color.soft_orange))

            toast.show()

        }
    }
}